const assert = require("assert");

const insertDocuments = function (db, callback) {
    // get the documents collection
    const Todos = db.collection("Todos");
    // Insert some documents
    Todos.insertMany(
        [
            {
                text: "Todo 4",
                completed: true
            },
            {
                text: "Todo 5",
                completed: false
            },
            {
                text: "Todo 6",
                completed: true
            }
        ],
        function (err, result) {
            assert.equal(err, null);
            assert.equal(3, result.result.n);
            assert.equal(3, result.ops.length);
            console.log("Inserted 3 documents into the collection");
            callback(result);
        }
    );
    //   db.collection('Users').insertOne({
    //       name: 'Andrew',
    //       age: 25,
    //       location: 'Philadelphia'
    //   }, function(err, result) {
    //     if (err) {
    //         return console.log('Unable insert user', err);
    //     }

    //     console.log(result.ops);
    //   })
};

const findDocuments = function (db, callback) {
    // Get the documents collection
    const collection = db.collection("Todos");
    // Find some documents
    collection.find({ completed: false }).toArray(function (err, docs) {
        assert.equal(err, null);
        console.log("Found the following records");
        callback(docs);
    });
};

const updateDocument = function (db, callback) {
    // Get the documents collection
    const collection = db.collection("Todos");
    // Update document where a is 2, set b equal to 1
    collection.updateOne(
        { text: "Somthing to do rrr" },
        { $set: { text: "Somthing to do" } },
        function (err, result) {
            assert.equal(err, null);
            assert.equal(1, result.result.n);
            console.log("Updated the document ");
            callback(result);
        }
    );
};

const removeDocument = function (db, callback) {
    // Get the documents collection
    const collection = db.collection("Todos");
    // Delete document where a is 3
    collection.deleteOne({ text: "Todo 6" }, function (err, result) { //deleteMany and findOneAndDelete
        assert.equal(err, null);
        // assert.equal(1, result.result.n);
        console.log("Removed the document ");
        callback(result);
    });
};

const indexCollection = function (db, callback) {
    db.collection("Todos").createIndex({ textDes: "Todo 9" }, null, function (
        err,
        results
    ) {
        console.log(results);
        callback();
    });
};

module.exports = {
    indexCollection,
    insertDocuments,
    findDocuments,
    updateDocument,
    removeDocument,
};