const connection = (callback) => {
  const { MongoClient, ObjectID } = require("mongodb");

  const assert = require("assert");
  //connection URL
  const url = "mongodb://localhost:27017/TodoApp";
  // Database Name
  const dbName = "TodoApp";

  // create a new MongoClient
  const client = new MongoClient(url, { useNewUrlParser: true });

  // Use connect method to connect to the server
  client.connect(function (err) {
    assert.equal(null, err);
    console.log("Connected successfully to server");
    const db = client.db(dbName);
    callback(db, client);
  });
};

module.exports = connection;

