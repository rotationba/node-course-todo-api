const connection = require('./playground/mongodb-connect');
const modules = require('./playground/modules');

connection((db, client)=>{
    modules.findDocuments(db, (res) => {
        console.log(res)
        client.close();
    })
});