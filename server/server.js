const express = require("express");
const bodyParser = require("body-parser");
var { mongoose } = require("./db/mongoose");

var routerTodos = require("./router/todos");
var routerUsers = require("./router/users");
var routerPaper = require('./router/paper');
var routerTapChi = require('./router/tapchi');
var app = express();

app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:3001");

    // Request methods you wish to allow
    res.setHeader(
        "Access-Control-Allow-Methods",
        "GET, POST, OPTIONS, PUT, PATCH, DELETE"
    );

    // Request headers you wish to allow
    res.setHeader(
        "Access-Control-Allow-Headers",
        "X-Requested-With,content-type"
    );

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader("Access-Control-Allow-Credentials", true);

    // Pass to next layer of middleware
    next();
});
const port = process.env.PORT || 3000;
app.use(bodyParser.json());

app.post("/todos", routerTodos);
app.get("/todos", routerTodos);
app.get("/todos/:id", routerTodos);
app.delete("/todos/:id", routerTodos);
app.patch("/todos/:id", routerTodos);

app.post("/papers", routerPaper);
app.get("/papers", routerPaper);
app.get("/papers/:id", routerPaper);
app.delete("/papers/:id", routerPaper);
app.patch("/papers/:id", routerPaper);

app.post("/tapchi", routerTapChi);
app.get("/tapchi", routerTapChi);
app.get("/tapchi/:id", routerTapChi);
app.delete("/tapchi/:id", routerTapChi);
app.patch("/tapchi/:id", routerTapChi);

app.get("/users", routerUsers);
app.post("/users", routerUsers);

app.listen(port, () => {
  console.log(`Started on port ${port}`);
});

module.exports = { app };
