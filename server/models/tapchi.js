var mongoose = require('mongoose');

var Tapchi = mongoose.model("Tapchi", {
  nameTapChi: {
    type: String,
    required: true,
    minlength: 1,
    trim: true,
    unique: true,
  }
});

module.exports = { Tapchi };