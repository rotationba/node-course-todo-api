var mongoose = require('mongoose');

var Paper = mongoose.model("Paper", {
  namePaper: {
    type: String,
    required: true,
    minlength: 1,
    trim: true,
    unique: true,
  },
  namXuatBan: {
    type: Number,
    required: true,
    minlength: 4,
    trim: true
  },
  idTapChi: {
    type: String,
    default: null
  }
});

module.exports = { Paper };