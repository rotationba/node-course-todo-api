const express = require('express');
const _ = require("lodash");
const router = express.Router();

var { User } = require("../models/user");


router.get("/users", (req, res) => {
    User.find().then(
    users => {
        res.send({ users });
    },
    e => {
      res.status(400).send(e);
    }
  );
});
router.post('/users', (req, res) => {
    var body = _.pick(req.body, ['email', 'password']);
    var user = new User(body);

    user.save().then(() => {
        return user.generateAuthToken();
        // res.send(user);
    }).then((token) => {
        res.header('x-auth', token).send(user);
    }).catch(e => res.status(404).send(e));
})

module.exports = router;
