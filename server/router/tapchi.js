const express = require("express");
const { ObjectID } = require("mongodb");
const _ = require("lodash");
const router = express.Router();
var { Tapchi } = require("../models/tapchi");
var { Paper } = require("../models/paper");

router.get("/tapchi", (req, res) => {
  Tapchi.find().then(
    tapchi => {
      res.send({ tapchi });
    },
    e => {
      res.status(400).send(e);
    }
  );
});

router.get("/tapchi/:id", (req, res) => {
  var id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  Tapchi.findById(id)
    .then(tapchi => {
      if (!tapchi) {
        return res.status(404).send();
      }

      res.send({ tapchi });
    })
    .catch(e => {
      res.status(404).send();
    });
});

router.post("/tapchi", (req, res) => {
  var tapchi = new Tapchi({
    nameTapChi: req.body.nameTapChi
  });

  tapchi.save().then(
    doc => {
      res.send(doc);
    },
    e => {
      res.status(400).send(e);
    }
  );
});

router.delete("/tapchi/:id", (req, res) => {
  var id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }
  console.log(id);
  Tapchi.findByIdAndDelete(id)
    .then(tapchi => {
      if (!tapchi) {
        return res.status(404).send();
      }
      Paper.deleteMany({ idTapChi: id })
        .then(res => console.log(res))
        .catch (e => console.log(e))
      res.send(tapchi);
    })
    .catch(e => res.status(404).send());
});

router.patch("/tapchi/:id", (req, res) => {
  var id = req.params.id;
  var body = _.pick(req.body, ["nameTapChi"]);
  //   console.log(body);
  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }
  Tapchi.findByIdAndUpdate(id, { $set: body }, { new: true })
    .then(tapchi => {
      if (!tapchi) {
        res.status(404).send();
      }

      res.send({ tapchi });
    })
    .catch(e => res.status(404).send());
});

module.exports = router;
