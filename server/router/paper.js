const express = require("express");
const { ObjectID } = require("mongodb");
const _ = require("lodash");
const router = express.Router();
var { Paper } = require("../models/paper");
var { Tapchi } = require("../models/tapchi");

router.get("/papers", (req, res) => {
  Paper.find().then(
     papers => {
       Promise.all(papers.map(paper => {
         if (!paper.idTapChi) {
           return new Promise((resolve, reject) => {
             resolve(paper)
           });
         } else {
            return Tapchi.findById(paper.idTapChi)
              .then(tapchi => {
                return {
                  ...paper._doc,
                  tapchi: tapchi,
                }
              })
         }
       }))
       .then(values => res.send({papers: values}));
    },
    e => {
      res.status(400).send(e);
    }
  );
});

router.get("/papers/:id", (req, res) => {
  var id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  Paper.findById(id)
    .then(paper => {
      if (!paper) {
        return res.status(404).send();
      } else {
        idTapChi = paper.idTapChi;
        Tapchi.findById(idTapChi)
        .then(tapchi => {
          if (!tapchi){
            return res.send({ paper });
          }
          let newPaper = { ...paper._doc, nameTapChi: tapchi.nameTapChi };
          res.send({ paper: newPaper });
        })
        .catch(e => res.status(404).send(e))
      }
    })
    .catch(e => {
      res.status(404).send();
    });
});

router.post("/papers", (req, res) => {
  Tapchi.findById(req.body.idTapChi)
    .then(tapchi => {
      if (!tapchi) {
        return res.send({ error: "KHONG_TON_TAI_ID_TAP_CHI_NAY" });
      }
    })
    .catch(e => res.send(e));

  var paper = new Paper({
    namePaper: req.body.namePaper,
    namXuatBan: req.body.namXuatBan,
    idTapChi: req.body.idTapChi
  });

  paper.save().then(
    doc => {
      res.send(doc);
    },
    e => {
      res.status(400).send(e);
    }
  );
});

router.delete("/papers/:id", (req, res) => {
  var id = req.params.id;
  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  Paper.findByIdAndDelete(id)
    .then(paper => {
      if (!paper) {
        return res.status(404).send();
      }

      res.send(paper);
    })
    .catch(e => res.status(404).send());
});

router.patch("/papers/:id", (req, res) => {
  var id = req.params.id;
  var body = _.pick(req.body, ["namePaper", "namXuatBan", "idTapChi"]);
  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  //   if (_.isBoolean(body.completed) && body.completed) {
  //     body.completedAt = new Date().getTime();
  //   } else {
  //     body.completed = false;
  //     body.completedAt = null;
  //   }

  Paper.findByIdAndUpdate(id, { $set: body }, { new: true })
    .then(paper => {
      if (!paper) {
        res.status(404).send();
      }

      res.send({ paper });
    })
    .catch(e => res.status(404).send());
});

module.exports = router;
